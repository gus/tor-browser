# Copyright (c) 2022, The Tor Project, Inc.
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Download pane warning
torbutton.download.warning.title = Be careful opening downloads
# %S will be a link to the Tails operating system website. With the content given by torbutton.download.warning.tails_brand_name
torbutton.download.warning.description = Some files may connect to the internet when opened without using Tor. To be safe, open the files while offline or use a portable operating system like %S.
# Locale name for Tails operating system.
torbutton.download.warning.tails_brand_name = Tails
torbutton.download.warning.dismiss = Got it

# .Onion Page Info prompt.
pageInfo_OnionEncryptionWithBitsAndProtocol=Connection Encrypted (Onion Service, %1$S, %2$S bit keys, %3$S)
pageInfo_OnionEncryption=Connection Encrypted (Onion Service)

# Shared between Onion Auth prompt and preferences
onionServices.learnMore=Learn more

# Onion Services Authentication prompt
# LOCALIZATION NOTE: %S will be replaced with the .onion address.
onionServices.authPrompt.description2=%S is requesting that you authenticate.
onionServices.authPrompt.keyPlaceholder=Enter your private key for this onion service
onionServices.authPrompt.done=Done
onionServices.authPrompt.doneAccessKey=d
onionServices.authPrompt.invalidKey=Please enter a valid key (52 base32 characters or 44 base64 characters)
onionServices.authPrompt.failedToSetKey=Unable to configure Tor with your key

# Onion Services Authentication preferences
onionServices.authPreferences.header=Onion Services Authentication
onionServices.authPreferences.overview=Some onion services require that you identify yourself with a key (a kind of password) before you can access them.
onionServices.authPreferences.savedKeys=Saved Keys…
onionServices.authPreferences.dialogTitle=Onion Service Keys
onionServices.authPreferences.dialogIntro=Keys for the following onionsites are stored on your computer
onionServices.authPreferences.onionSite=Onionsite
onionServices.authPreferences.onionKey=Key
onionServices.authPreferences.remove=Remove
onionServices.authPreferences.removeAll=Remove All
onionServices.authPreferences.failedToGetKeys=Unable to retrieve keys from tor
onionServices.authPreferences.failedToRemoveKey=Unable to remove key

# Onion services error strings.
onionServices.errorPage.browser=Browser
onionServices.errorPage.network=Network
onionServices.errorPage.onionSite=Onionsite
# LOCALIZATION NOTE: In the longDescription strings, %S will be replaced with
#                    an error code, e.g., 0xF3.
# Tor SOCKS error 0xF0:
onionServices.descNotFound.pageTitle=Problem Loading Onionsite
onionServices.descNotFound.header=Onionsite Not Found
onionServices.descNotFound=The most likely cause is that the onionsite is offline. Contact the onionsite administrator.
onionServices.descNotFound.longDescription=Details: %S — The requested onion service descriptor can't be found on the hashring and therefore the service is not reachable by the client.
# Tor SOCKS error 0xF1:
onionServices.descInvalid.pageTitle=Problem Loading Onionsite
onionServices.descInvalid.header=Onionsite Cannot Be Reached
onionServices.descInvalid=The onionsite is unreachable due an internal error.
onionServices.descInvalid.longDescription=Details: %S — The requested onion service descriptor can't be parsed or signature validation failed.
# Tor SOCKS error 0xF2:
onionServices.introFailed.pageTitle=Problem Loading Onionsite
onionServices.introFailed.header=Onionsite Has Disconnected
onionServices.introFailed=The most likely cause is that the onionsite is offline. Contact the onionsite administrator.
onionServices.introFailed.longDescription=Details: %S — Introduction failed, which means that the descriptor was found but the service is no longer connected to the introduction point. It is likely that the service has changed its descriptor or that it is not running.
# Tor SOCKS error 0xF3:
onionServices.rendezvousFailed.pageTitle=Problem Loading Onionsite
onionServices.rendezvousFailed.header=Unable to Connect to Onionsite
onionServices.rendezvousFailed=The onionsite is busy or the Tor network is overloaded. Try again later.
onionServices.rendezvousFailed.longDescription=Details: %S — The client failed to rendezvous with the service, which means that the client was unable to finalize the connection.
# Tor SOCKS error 0xF4:
onionServices.clientAuthMissing.pageTitle=Authorization Required
onionServices.clientAuthMissing.header=Onionsite Requires Authentication
onionServices.clientAuthMissing=Access to the onionsite requires a key but none was provided.
onionServices.clientAuthMissing.longDescription=Details: %S — The client downloaded the requested onion service descriptor but was unable to decrypt its content because client authorization information is missing.
# Tor SOCKS error 0xF5:
onionServices.clientAuthIncorrect.pageTitle=Authorization Failed
onionServices.clientAuthIncorrect.header=Onionsite Authentication Failed
onionServices.clientAuthIncorrect=The provided key is incorrect or has been revoked. Contact the onionsite administrator.
onionServices.clientAuthIncorrect.longDescription=Details: %S — The client was able to download the requested onion service descriptor but was unable to decrypt its content using the provided client authorization information. This may mean that access has been revoked.
# Tor SOCKS error 0xF6:
onionServices.badAddress.pageTitle=Problem Loading Onionsite
onionServices.badAddress.header=Invalid Onionsite Address
onionServices.badAddress=The provided onionsite address is invalid. Please check that you entered it correctly.
onionServices.badAddress.longDescription=Details: %S — The provided .onion address is invalid. This error is returned due to one of the following reasons: the address checksum doesn't match, the ed25519 public key is invalid, or the encoding is invalid.
# Tor SOCKS error 0xF7:
onionServices.introTimedOut.pageTitle=Problem Loading Onionsite
onionServices.introTimedOut.header=Onionsite Circuit Creation Timed Out
onionServices.introTimedOut=Failed to connect to the onionsite, possibly due to a poor network connection.
onionServices.introTimedOut.longDescription=Details: %S — The connection to the requested onion service timed out while trying to build the rendezvous circuit.

# Profile/startup error messages.
# LOCALIZATION NOTE: %S is the application name.
profileProblemTitle=%S Profile Problem
profileReadOnly=You cannot run %S from a read-only file system.  Please copy %S to another location before trying to use it.
profileReadOnlyMac=You cannot run %S from a read-only file system.  Please copy %S to your Desktop or Applications folder before trying to use it.
profileAccessDenied=%S does not have permission to access the profile. Please adjust your file system permissions and try again.
